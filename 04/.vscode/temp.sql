SHOW DATABASES;

DROP DATABASE IF EXISTS db_people;
CREATE DATABASE db_people;
USE db_people;

CREATE TABLE tbl_people (
  ID int(11) NOT NULL AUTO_INCREMENT,
  NAME varchar(50) NOT NULL,
  EMAIL varchar(50) NOT NULL,
  COUNTRY varchar(20) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

Select * from tbl_people;

INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Kai","vitae.mauris.sit@fermentumarcu.net","Guadeloupe");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Joelle","odio@duiCum.org","Belize");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Brenna","porttitor.vulputate@malesuadaIntegerid.org","Brunei");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Neville","et.magnis.dis@Integer.edu","French Guiana");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Magee","tum.sociis.natoque@etrutrumnon.ca","Barbados");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Blake","nulla@senectus.edu","Cook Islands");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Adrienne","ac.eleifend@Cras.ca","Niue");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Dexter","sapien@nulla.co.uk","Brunei");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Jillian","ac@maurissagittis.edu","Malta");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Amir","dis.parturient.montes@nonquam.edu","French Guiana");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Rhea","et.libero.Proin@maurisidsapien.edu","Nauru");
INSERT INTO `tbl_people` (`NAME`,`EMAIL`,`COUNTRY`) VALUES ("Lani","adipiscing@afelis.net","Chad");

Select * from tbl_people;