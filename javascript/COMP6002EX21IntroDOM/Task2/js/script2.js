function changeImage(number)
{
    var num = parseInt(number);

    if(num < 10)
    {
        document.getElementById("mainimage").setAttribute("src", "http://lorempixel.com/400/400/sports/"+number);
        document.getElementById("link").innerHTML = document.getElementById("mainimage").getAttribute("src");
    }
    else
    {
        document.getElementById("link").innerHTML = "Please choose a number lower than 10";
    }
}