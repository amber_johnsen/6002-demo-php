function change_color(colour)
{
    document.body.style.backgroundColor = colour;
}

function message()
{
    var buttons = document.getElementsByTagName("Button");

    for(var i = 0; i < buttons.length; i++)
    {
        buttons[i].innerHTML = "Click me!";
    }
}

function hidemessage()
{
    var buttons = document.getElementsByTagName("Button");

    buttons[0].innerHTML = "Black";
    buttons[1].innerHTML = "Red";
    buttons[2].innerHTML = "Yellow";
    buttons[3].innerHTML = "White";
}